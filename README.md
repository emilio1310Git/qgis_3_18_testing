# QGIS 3.18 Testing

Recursos e información de las pruebas realizadas por miembros de la Asociación QGIS España para QGIS 3.18.0

## Objetivo

Actividad organizada la Asociación QGIS España probar y documentar las características de la version QGIS 3.18.0 Zürich (2021-02-19)

El listado de novedades se puede consultar en el siguiente enlace https://qgis.org/en/site/forusers/visualchangelog318/index.html

## Participantes
- Juanma Martín Linkedin -  Twitter - Web
- José Ruiz Manzanares [Linkedin](https://www.linkedin.com/in/jos%C3%A9-ruiz-manzanares-72854a42/) -  [Twitter](https://twitter.com/JoseRuizMan) - Web
- Emilio Sanchez Linkedin -  Twitter - Web
- Josep Lluís Sala [Linkedin](https://www.linkedin.com/in/joseplluissala/) -  [Twitter](https://twitter.com/jllsala) - Web
- David García Hernández [Linkedin](https://www.linkedin.com/in/david-garc%C3%ADa-hern%C3%A1ndez-9336a915/) -  Twitter - Web
- Lucho Ferrer Linkedin -  Twitter - Web
- Patricio Soriano Castro [Linkedin](https://www.linkedin.com/in/patriciosorianocastro/) -  [Twitter](https://twitter.com/sigdeletras) - [Web](http://sigdeletras.com/)

## Características

### 01 User Interface - Juanma Martín

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#user-interface)

- Feature: Hide derived attributes from the Identify results
- Feature: Close all tabs at once from message logs interface
- Feature: API for layer source widgets
- Feature: GUI for dynamic SVGs
- Feature: Zoom and pan to selection for multiple layers
- Feature: Zoom in/out by scrolling mouse wheel over map overview panel

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/01](/recursos/01)

![GUI for dynamic SVGs](/recursos/01/GUI_for_dynamic_SVGs.mp4)

### 02 Accessibility Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#accessibility)

- Feature: Improved color vision deficiency simulation	
- Feature: Rotation widget for the Georeferencer

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/01](/recursos/02)

![Rotation widget for the Georeferencer](/recursos/02/Georeferencer.mp4)

### 03 Symbology José Ruiz Manzanares

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#symbology)

- Feature: Data defined overall symbol opacity
- Feature: Open the style gallery from the style manager

**Comentarios**

**Recursos**

### 04 Mesh Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#mesh)

- Feature: New mesh export algorithms
- Feature: Native export for mesh layers
- Feature: Mesh simplification for 3D
- Feature: Multiple native mesh processing algorithms

**Comentarios**

*QGIS sigue incorporando mejoras para el trabajo de datos de tipo malla incluyendo las funcionalidades sin dependencias externas en el mismo core de la aplicación.*

*Desde la Caja de Herramientas de procesamiento podremos usar nuevas herramientas de exportación de caras, bordes o cuadrículas.*

*Si queremos consultar los datos de mallas en una vista 3D podremos definir el nivel de detalle de la renderización que estará directamente relacionado con el rendimiento de la carga de la vista.*

**Recursos**

Carpeta de recursos [/recursos/04](/recursos/04)

![New mesh export algorithms](/recursos/04/New_mesh_export_algorithms.mp4)

![Multiple native mesh processing algorithms](/recursos/04/NMultiple_native_mesh_processing_algorithms.mp4)

### 05 Rendering *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#rendering)

- Feature: “Merged feature” renderer for polygon and line layers
- Feature: Smarter Map Redraws

### 06 3D Features Emilio Sanchez

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#d-features)

- Feature: Eye dome lighting
- Feature: Data defined 3D material colors
- Feature: 3D Orthographic projection support

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/06](/recursos/06)

![3D_Features](/recursos/06/3D_Features.mp4)

### 07 Point Clouds Damián Ortega

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#point-clouds)

- Feature: Point Cloud Support
- Feature: Add point clouds to browser
- Feature: Untwine PDAL Provider Integration

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/07](/recursos/07)

### 08 Print Layouts Josep Lluís Sala

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#print-layouts)

- Feature: Gradient ramp based legends
- Feature: Color ramp legend improvements
- Feature: Dynamic text presets

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/06](/recursos/08)

![Gradient ramp based legends](/recursos/08/Gradient_ramp_based_legends.mp4)

![Color ramp legend improvements](/recursos/08/Color_ramp_legend_improvements.mp4)

![Dynamic text presets](/recursos/08/Dynamic_text_presets.mp4)
### 09 Expressions *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#expressions)

- Feature: Optional formatting of UUID results	
- Feature: Layer CRS variable for expressions	
- Feature: Support for min, max, majority, sum, mean, and median functions on numerical arrays	
- Feature: Negative index for array_get function	
- Feature: Add map_credits function	

### 10 Digitising Josep Lluís Sala

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#digitising)

- Feature: Select features context menu
- Feature: Curve tracing settings added to UI
- Feature: Feature scaling tool

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/06](/recursos/10)

![Select features context menu](/recursos/10/Select_features_context_menu.mp4)

![Feature scaling tool](/recursos/10/Feature_scaling_tool.mp4)

### 11 Data Management	David García Hdez	

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#data-management)

- Feature: New export to spreadsheet algorithm
- Feature: Reproject coordinates in the Georeferencer
- Feature: Polymorphic relations/ Document management system	

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/11](/recursos/11)

### 12 Forms and Widgets *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#forms-and-widgets)

- Feature: Soft and hard constraints in forms

### 13 Analysis Tools Lucho Ferrer

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#analysis-tools)

- Feature: Nominatim geocoder API

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/13](/recursos/13)

### 14 Processing *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#processing)

- Feature: Allow expression for order fields in PointsToPath algorithm	
- Feature: Override CRS for Clip Raster by extent output	
- Feature: Add “retain fields” algorithm	David García Hdez
- Feature: Reference common field parameter for multiple layers	
- Feature: Extend import geotagged photos to include exif_orientation	
- Feature: Export layer information algorithm	
- Feature: Cell stack percentile and percentrank algorithms	
- Feature: Points to lines processing algorithm	

### 15 Application and Project Options	 Emilio Sanchez

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#application-and-project-options)

- Feature: Hidden layers
- Feature: Custom “Full Extent” definition
- Feature: Toggle network caching to QgsNetworkAccessManager
- 
**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/13](/recursos/13)

### 16 Browser	Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#browser)

- Feature: Unify ArcGis Feature Service and ArcGIS Map Service connections in browser	
- Feature: Allow browsing ArcGIS REST by content groups

**Comentarios**

Desde QGIS podemos conectarnos a servicios de ArcGIS, en concreto a los servicios de entidades (Feature Service) y de mapas (Map Service). Ahora el acceso a ambos se encuentra unificado en el Administrador de fuentes de datos bajo el nombre ArcGIS REST Server. Para diferenciar el tipo de servicios se usa simbología diferente.

**Recursos**

Carpeta de recursos [/recursos/16](/recursos/16)

Ejempplo de ArcGIS REST Sever.

[https://ciudadinteligente.granada.org/arcgis/rest/services/IDE_Publico/2018_CENTRO_NIVELES_PROTECCION_ARQUEOLOGICA/MapServer](https://ciudadinteligente.granada.org/arcgis/rest/services/IDE_Publico/2018_CENTRO_NIVELES_PROTECCION_ARQUEOLOGICA/MapServer)

![Unify ArcGis Feature Service and ArcGIS Map Service connections in browser](/recursos/16/Unify_ArcGis_Feature_Service_and_ArcGIS_Map_Service_connections_in_browser.mp4)
### 17 Data Providers	Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#data-providers)

- Feature: Native DXF export algorithm [Bug](https://github.com/qgis/QGIS/issues/41858)
- Feature: Additional geometry types for PostGIS Export
- Feature: Improved network requests with GDAL
- Feature: Read only generated fields
- Feature: Improve MSSQL loading with predefined parameters
- Feature: Filter schemas for MS SQL
- Feature: SAP HANA database support
- Feature: Deprecate support for DB2
- Feature: Oracle connection API
- Feature: Add advanced options for raster data imports

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/17](/recursos/17)

### 18 QGIS Server *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#qgis-server)

- Feature: GetLegendGraphics Symbol Scale	
- Feature: Drag and drop for WMS GetFeatureInfo response	

### 19 Programmability	*No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#programmability)

- Feature: Run multiple items from command history dialog	
- Feature: Enable or disable plugins from the command line	
